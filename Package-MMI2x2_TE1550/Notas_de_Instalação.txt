Notas de Instalação:

1) Instalar o arquivo .cml no INTERCONNECT na pasta Design Kits

> Botão direito em cima da pasta "Design Kits">>"Install"
> Selecionar o arquivo .cml e a pasta referente


(Caso o arquivo .cml acompanhe o arquivo .gds)

2) Referenciar o GDS ao arquivo .cml

> Abrir o .gds e salvar na pasta "C:\Users\USERNAME\KLayout\libraries"
> Nas opções de salvamento, colocar no espaço "Library name" o nome da biblioteca "Design Kits"